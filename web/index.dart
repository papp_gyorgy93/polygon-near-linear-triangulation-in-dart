import 'package:pnltrid/polygon_near_linear_triangulation.dart';
import 'dart:js';

void main(){
    //PNLTRI pnltri = new PNLTRI();

    Triangulator triangulator = new Triangulator();

    /*[ { x: 2.5, y:26.0 },   { x: 5.0, y:31.0 }, { x:10.0, y:28.5 } ],
    [ { x:27.5, y:25.0 },   { x:25.0, y:30.0 }, { x:20.0, y:27.5 } ],
    [ { x:15.0, y:20.0 },   { x:12.5, y:10.0 }, { x:18.0, y:10.0 } ],*/

    var vertices = [
      [
        JsPoint(0.0, 0.0),
        JsPoint(30.0, 0.0),
        JsPoint(30.0, 40.0),
        JsPoint(0.0, 4.0),
      ], [
        JsPoint(2.5, 26.0),
        JsPoint(5.0, 31.0),
        JsPoint(10.0, 28.5)
      ], [
        JsPoint(27.5, 25.0),
        JsPoint(25.0, 30.0),
        JsPoint(20.0, 27.5)
      ], [
        JsPoint(15.0, 20.0),
        JsPoint(12.5, 10.0),
        JsPoint(18.0, 10.5)
      ],
    ];

    var result = triangulator.triangulate_polygon(
        vertices, true);
    print(result);
}

JsObject JsPoint(double x, double y){
  var object = new JsObject(context['Object']);
  object["x"] = x;
  object["y"] = y;
  return object;
}