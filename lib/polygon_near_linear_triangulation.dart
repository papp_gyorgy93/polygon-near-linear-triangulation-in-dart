@JS()
library PNLTRI;

import 'package:js/js.dart';

@JS("PNLTRI.Triangulator")
class Triangulator{
  external factory Triangulator();
  external List<int> triangulate_polygon(List<Map<String, double>> inPolygonChains, bool inForceTrapezoidation);
}